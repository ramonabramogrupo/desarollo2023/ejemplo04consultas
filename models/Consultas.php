<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "consultas".
 *
 * @property int $id
 * @property string|null $texto
 * @property string|null $tabla
 * @property string|null $sql
 * @property resource|null $resultados
 */
class Consultas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consultas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['resultados'], 'string'],
            [['texto', 'tabla'], 'string', 'max' => 255],
            [['sql'], 'string', 'max' => 800],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'texto' => 'Texto',
            'tabla' => 'Tabla',
            'sql' => 'Sql',
            'resultados' => 'Resultados',
        ];
    }
}
