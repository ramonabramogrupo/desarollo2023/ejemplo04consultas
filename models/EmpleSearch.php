<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Emple;

/**
 * EmpleSearch represents the model behind the search form of `app\models\Emple`.
 */
class EmpleSearch extends Emple
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_no', 'dir', 'salario', 'comision', 'dept_no'], 'integer'],
            [['apellido', 'oficio', 'fecha_alt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Emple::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'emp_no' => $this->emp_no,
            'dir' => $this->dir,
            'fecha_alt' => $this->fecha_alt,
            'salario' => $this->salario,
            'comision' => $this->comision,
            'dept_no' => $this->dept_no,
        ]);

        $query->andFilterWhere(['like', 'apellido', $this->apellido])
            ->andFilterWhere(['like', 'oficio', $this->oficio]);

        return $dataProvider;
    }
}
