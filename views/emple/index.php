<?php

use app\models\Emple;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\EmpleSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Emples';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emple-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Emple', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'emp_no',
            'apellido',
            'oficio',
            'dir',
            'fecha_alt',
            //'salario',
            //'comision',
            //'dept_no',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Emple $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'emp_no' => $model->emp_no]);
                 }
            ],
        ],
    ]); ?>


</div>
