<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\EmpleSearch $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="emple-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'emp_no') ?>

    <?= $form->field($model, 'apellido') ?>

    <?= $form->field($model, 'oficio') ?>

    <?= $form->field($model, 'dir') ?>

    <?= $form->field($model, 'fecha_alt') ?>

    <?php // echo $form->field($model, 'salario') ?>

    <?php // echo $form->field($model, 'comision') ?>

    <?php // echo $form->field($model, 'dept_no') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
