<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>
<div class="emple-index">

    <h1><?= $titulo ?></h1>
    <h2><?= $texto?> </h2>

    <?php
       
        if(!is_null($datos)){
            if(($datos->count)>0){
                echo GridView::widget([
                    'dataProvider' => $datos,
                    'columns'=> $columnas,
                ]);
            }else{
                echo "no hay registros";
            }
        }
    ?>
    
    
</div>
