<?php

use app\models\Consultas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\ConsultasSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Consultas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consultas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'pager' => [
            'firstPageLabel' => 'Primera',
            'lastPageLabel'  => 'Última'
        ],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'texto',
            //'tabla',
            ['class' => 'yii\grid\ActionColumn',
                    'header' => 'Consultas',
                    'template' => '{link}',
                    'buttons' => [
                        'link' => function ($url,$model) {
                            return Html::a(Html::a('Consulta', ['/consultas/consulta','id'=>$model->id],
                                    ['class'=>'btn btn-danger']) ,
                                $url);
                        },
            ],
        ],
    ],
    ]); ?>


</div>
