<?php

/* @var $this yii\web\View */

use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = $titulo;
?>
<div style="margin-top:50px;"> </div>
<div class="site-about pdfSalida">
    <div class="row">
        <div style="float:none" class="col-lg-8 center-block">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h2 class="panel-title">
                        <?= $this->title ?>
                    </h2>
                </div>
                <div style="height: 600px" class="panel-body">
                    <?= $descripcion ?>
                
                    <div style="margin:30px;float:none" class="col-lg-6 center-block">
                            <div class="list-group">
                                <div class="list-group-item list-group-item-info">Las consultas a resolver:</div>

                                <?php
                                foreach ($consultas as $consulta){
                                        echo '<div class="list-group-item"><span style="line-height:20px;height:20px;border-radius:5px;width:60px;display:block;float:left" class="bg-info">' . "&nbsp;&nbsp;" . $consulta->id . "&nbsp;&nbsp;" . '</span><span stype="display:block;float:left">' . $consulta->texto . '</span></div>';
                                }
                                ?>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div style="display: block; page-break-before: always;"> </div>

    <table class="tablas">
    <?php
    foreach($consultas as $consulta){
    ?>
    <tr>
        <td><?= $consulta->id ?></td>
        <td><?= $consulta->texto ?></td>
    </tr>
    <tr>
        <td colspan="2"><?= $consulta->sql ?></td>
    </tr>
    
    <?php
        
    }
    ?>
    </table>
</div>
