<?php

/** @var yii\web\View $this */

$this->title = 'Consultas';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4">Consultas</h1>

        <p class="lead">Aplicacion para probar consultas</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6 mb-3 text-center">
                <h2>Empleados</h2>
            </div>
            <div class="col-lg-6 mb-3 text-center">
                <h2>Departamentos</h2>
            </div>
        </div>
    </div>
</div>
